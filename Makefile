proj := mmdata_mgr
proj-major := 1
proj-minor := 0
proj-version := $(proj-major).$(proj-minor)

CFLAGS ?= -g
CFLAGS += -Wall

prefix := /usr/local
bindir := $(prefix)/bin
libdir := $(prefix)/lib
includedir := $(prefix)/include

ifneq ($(CROSS_COMPILE),)
CC := $(CROSS_COMPILE)gcc
endif
SFLAGS := -I$(shell $(CC) -print-file-name=include) -Wno-non-pointer-null

#17
$(proj)-srcs := \
	src/mmdata_main.c \
	src/mmdata_rmnet.c

$(proj)-cflags := -Iinc

$(proj)-lflags := -lpthread -lqrtr -lrmnetctl

LDFLAGS += -lpthread -lqrtr -lrmnetctl

targets :=  $(proj)

out := out
src_to_obj = $(patsubst %.c,$(out)/obj/%.o,$(1))
src_to_dep = $(patsubst %.c,$(out)/dep/%.d,$(1))

all-srcs :=
all-objs :=
all-deps :=
all-clean := $(out)
all-install :=

all: $(targets)

$(out)/obj/%.o: %.c
ifneq ($C,)
	@echo "CHECK	$<"
	@sparse $< $(patsubst -iquote=%,-I%,$(CFLAGS)) $(SFLAGS)
endif
	@echo "CC	$<"
	@$(CC) -MM -MF $(call src_to_dep,$<) -MP -MT "$@ $(call src_to_dep,$<)" $(CFLAGS) $(_CFLAGS) $<
	@$(CC) -o $@ -c $< $(CFLAGS) $(_CFLAGS)

define add-inc-target
$(DESTDIR)$(includedir)/$2: $1/$2
	@echo "INSTALL	$$<"
	@install -D -m 755 $$< $$@

all-install += $(DESTDIR)$(includedir)/$2
endef

define add-target-deps
all-srcs += $($1-srcs)
all-objs += $(call src_to_obj,$($1-srcs))
all-deps += $(call src_to_dep,$($1-srcs))
all-clean += $1
$(call src_to_obj,$($1-srcs)): _CFLAGS := $($1-cflags)
endef

define add-bin-target

$(call add-target-deps,$1)

$1: $(call src_to_obj,$($1-srcs))
	@echo "LD	$$@"
	@echo "LD	$$@"
	$$(CC) -o $$@ $$(filter %.o,$$^) $(LDFLAGS)

$(DESTDIR)$(bindir)/$1: $1
	@echo "INSTALL	$$<"
	@install -D -m 755 $$< $$@

all-install += $(DESTDIR)$(bindir)/$1
endef

define add-lib-target

$(call add-target-deps,$1)

$1: $(call src_to_obj,$($1-srcs))
	@echo "LD	$$@"
	$$(CC) -o $$@ $$(filter %.o,$$^) -shared -Wl,-soname,$1.$(proj-major)

$(DESTDIR)$(libdir)/$1.$(proj-version): $1
	@echo "INSTALL	$$<"
	@install -D -m 755 $$< $$@
	@ln -sf $1.$(proj-version) $(DESTDIR)$(libdir)/$1.$(proj-major)
	@ln -sf $1.$(proj-major) $(DESTDIR)$(libdir)/$1

all-install += $(DESTDIR)$(libdir)/$1.$(proj-version)
endef

$(foreach v,$(filter-out %.so,$(targets)),$(eval $(call add-bin-target,$v)))
$(foreach v,$(filter %.so,$(targets)),$(eval $(call add-lib-target,$v)))

install: $(all-install)

clean:
	@echo CLEAN
	@$(RM) -r $(all-clean)

$(call src_to_obj,$(all-srcs)): Makefile

ifneq ("$(MAKECMDGOALS)","clean")
cmd-goal-1 := $(shell mkdir -p $(sort $(dir $(all-objs) $(all-deps))))
-include $(all-deps)
endif
