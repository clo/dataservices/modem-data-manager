/******************************************************************************

Copyright (c) 2020-2021 The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of The Linux Foundation nor the names of its
          contributors may be used to endorse or promote products derived
          from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#include "mmdata_rmnet.h"
#include <librmnetctl.h>
#include <string.h>
#include <fcntl.h>

int
mmdata_rmnet_configure_links
(
  void
)
{
  int rc;
  uint16_t err;
  rmnetctl_hndl_t *rmnet_cfg_handle;

  if (RMNETCTL_SUCCESS != (rc = rtrmnet_ctl_init(&rmnet_cfg_handle, &err)))
  {
    MMDATA_ERR("%s(): Error on rtrmnet lib init rc =%d\n", __func__,rc);
  }

  if (RMNETCTL_SUCCESS != (rc = rtrmnet_ctl_newvnd(rmnet_cfg_handle,
                                              MMDATA_RMNET_IPA,
                                              MMDATA_RMNET_0, &err, 1, 0)))
  {
    MMDATA_ERR("%s(): Error on rtrmnet_ctl_newvnd rc=%d err=%d\n", __func__, rc, err);
  }

  rtrmnet_ctl_deinit(rmnet_cfg_handle);

  return MMDATA_SUCCESS;
}

int
mmdata_rmnet_change_link_configuration
(
  int flag
)
{

  uint16_t err;
  rmnetctl_hndl_t *rmnet_cfg_handle;

  if (RMNETCTL_SUCCESS != rtrmnet_ctl_init(&rmnet_cfg_handle, &err))
  {
    MMDATA_ERR("%s(): Error on rtrmnet lib init ", __func__);
  }

  if (RMNETCTL_SUCCESS != rtrmnet_ctl_changevnd(rmnet_cfg_handle,
                                            MMDATA_RMNET_IPA,
                                            MMDATA_RMNET_0 , &err, 1, flag))
  {
    MMDATA_ERR("%s(): Error on rtrmnet lib init Error code:%d", __func__, err);
  }
  else
  {
    MMDATA_INFO("%s(): Link changed  with flag %d", __func__, flag);
  }

  rtrmnet_ctl_deinit(rmnet_cfg_handle);

   return MMDATA_SUCCESS;
}

int
mmdata_rmnet_remove_links
(
  void
)
{
  uint16_t err;
  rmnetctl_hndl_t *rmnet_cfg_handle;

  if (RMNETCTL_SUCCESS != rtrmnet_ctl_init(&rmnet_cfg_handle, &err))
  {
    MMDATA_ERR("%s(): Error on rtrmnet lib init ", __func__);
  }

  if (RMNETCTL_SUCCESS != rtrmnet_ctl_delvnd(rmnet_cfg_handle, MMDATA_RMNET_0 , &err))
  {
    MMDATA_ERR("%s(): Error on rtrmnet lib init Error code:%d", __func__, err);
  }

  rtrmnet_ctl_deinit(rmnet_cfg_handle);

  return MMDATA_SUCCESS;
}

static int
mmdata_rmnet_update_config
(
  const char    *filename,
  mmdata_procsys_cfg_t *cfg
)
{
  FILE *fp = NULL;
  int rc   = MMDATA_FAILURE;
  int reti = MMDATA_SUCCESS;

  if (!filename || !cfg)
  {
    MMDATA_ERR("%s(): invalid params!", __func__);
    goto bail;
  }

  /* Check if we can access the procfs/sysfs file */
  if (access(filename, F_OK))
  {
    MMDATA_ERR("%s(): unable to access %s file. Err [%d:%s]", __func__,
                 filename, errno, strerror(errno));
    goto bail;
  }

  fp = fopen(filename, "w");
  if (!fp)
  {
    MMDATA_ERR("%s(): unable to open %s file. Err [%d:%s]", __func__,
                 filename, errno, strerror(errno));
    goto bail;
  }

  switch (cfg->config_type)
  {
  case MMDATA_TYPE_INT:
    fprintf(fp, "%d", cfg->config_val.intval);
    break;
  case MMDATA_TYPE_HEX:
    fprintf(fp, "%x", cfg->config_val.intval);
    break;
  case MMDATA_TYPE_LONG:
    fprintf(fp, "%lu", cfg->config_val.longval);
    break;
  case MMDATA_TYPE_STR:
    fprintf(fp, "%s", cfg->config_val.strval);
    break;
  case MMDATA_TYPE_INVALID:
  case MMDATA_TYPE_MAX:
  default:
    MMDATA_ERR("%s(): unknown value type!", __func__);
    reti = MMDATA_FAILURE;
    break;
  }

  if (MMDATA_SUCCESS != reti)
  {
    goto bail;
  }

  rc = MMDATA_SUCCESS;

bail:
  if (fp)
  {
    if (fclose(fp))
    {
      MMDATA_ERR("%s(): unable to close %s file! Err [%d:%s]", __func__,
                   filename, errno, strerror(errno));
    }
  }
  return rc;
}

static void
mmdata_rmnet_set_ipv6_config
(
  const char *link,
  const char *property,
  const char *value
)
{
  char proc_file[MMDATA_MAX_PROC_LEN] = "";
  mmdata_procsys_cfg_t proc_cfg;

  if (!link || !property || !value)
    return;

  snprintf(proc_file, sizeof(proc_file), "/proc/sys/net/ipv6/conf/%s/%s", link, property);

  mmdata_strcpy(proc_cfg.config_val.strval, value, sizeof(proc_cfg.config_val.strval));

  proc_cfg.config_type = MMDATA_TYPE_STR;

  if (MMDATA_SUCCESS != mmdata_rmnet_update_config(proc_file, &proc_cfg))
  {
    MMDATA_ERR("%s(): failed to configure IPv6 property %s for link [%s]",
                 __func__, property, link);
  }
}
static int
mmdata_check_rx_offload
()
{
  char sysfs_file[MMDATA_MAX_PROC_LEN] = "";
  char buf[1024] = "";
  ssize_t len;
  snprintf(sysfs_file, sizeof(sysfs_file), "/sys/devices/platform/soc@0/1e40000.ipa/feature/rx_offload");

  int fd;
  fd = open(sysfs_file, O_RDONLY);
  if(fd < 0)
  {
     MMDATA_ERR("%s(): failed to open sysfs file for rx checksum", __func__);
     goto bail;
  }

  len = read(fd, buf, sizeof(buf)-1);
  if (len < 0)
  {
     MMDATA_ERR("%s(): failed to read sysfs file for rx checksum", __func__);
     goto bail;
  }
  buf[len] = 0;
  if(strncmp(buf, MMDATA_QMAPv5_STR, strlen(buf)))
     return MMDATA_QMAPv5;

bail:
  return MMDATA_QMAPv4;
}
static int
mmdata_rmnet_enable
(
  const char *dev_name
)
{
  int fd;
  int rval = MMDATA_FAILURE;
  struct ifreq ifr;

  MMDATA_ASSERT(dev_name);

  if((fd = socket(AF_INET, SOCK_DGRAM | SOCK_CLOEXEC, 0)) < 0)
  {
    MMDATA_ERR("ifioctl_set: socket failed");
    goto error;
  }

  memset(&ifr, 0, sizeof(ifr));
  if (mmdata_strcpy(ifr.ifr_name, dev_name, sizeof(ifr.ifr_name)) >= sizeof(ifr.ifr_name))
  {
    MMDATA_ERR("%s(): String truncation occurred", __func__);
    close(fd);
    goto error;
  }

  ifr.ifr_flags |= IFF_UP;
  if(ioctl(fd, SIOCSIFFLAGS, &ifr) < 0)
  {
    MMDATA_ERR("ifioctl_set: SIOCSIFFLAGS ioctl failed");
    close(fd);
    goto error;
  }

  close(fd);
  rval = MMDATA_SUCCESS;

error:
  MMDATA_INFO("%s(): device UP on [%s] : [%s]", __func__, dev_name,
                rval == MMDATA_SUCCESS ? "succces" : "failure");
  return rval;
}

int mmdata_rmnet_cfg (void)
{
  int rc = MMDATA_FAILURE;

  mmdata_rmnet_set_ipv6_config(MMDATA_RMNET_IPA,"disable_ipv6","1");
  do
  {
    if (mmdata_rmnet_enable(MMDATA_RMNET_IPA) != MMDATA_SUCCESS)
    {
      MMDATA_ERR("%s(): Error putting transport in up state sleeping ...\n", __func__);
      sleep(1);
      continue;
    }
    else
    {
      MMDATA_ERR("%s(): sucess rmnet is in up state \n", __func__);
      break;
    }
  }
  while (true);

  if (MMDATA_SUCCESS != (rc = mmdata_rmnet_configure_links()))
  {
    MMDATA_ERR("mmdata_rmnet_configure_links config failed rc=%d\n",rc);
    return MMDATA_FAILURE;
  }
  else
  {
    MMDATA_INFO("link configured rc=%d\n",rc);
    int rx_offload = mmdata_check_rx_offload();
    MMDATA_INFO("QMAP version is : QMAP%d", rx_offload);
    if(rx_offload == MMDATA_QMAPv5)
    {
      mmdata_rmnet_change_link_configuration(49);
    }
    else
    {
      mmdata_rmnet_change_link_configuration(13);
    }
    mmdata_rmnet_set_ipv6_config(MMDATA_RMNET_0,"accept_ra","2");

  }
  return MMDATA_SUCCESS;
}
