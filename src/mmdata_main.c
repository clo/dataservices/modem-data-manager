/******************************************************************************

Copyright (c) 2020 The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of The Linux Foundation nor the names of its
          contributors may be used to endorse or promote products derived
          from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/

/*===========================================================================
                              INCLUDE FILES
===========================================================================*/
#include "mmdata_main.h"
#include "mmdata_rmnet.h"
/*===========================================================================
                     LOCAL DEFINITIONS AND DECLARATIONS
===========================================================================*/

FILE* log_file = NULL;
#define LOG_FILE_PATH "/var/spool/mmdata.log"

int
main (int argc, char *argv[])
{

  log_file = stdout;
  if( TRUE && argc==1)
  {
    // Initialize the log - use stdout as fallback
    log_file = fopen(LOG_FILE_PATH, "w+");
    if(!log_file)
    {
      log_file = stdout;
    }
    MMDATA_INFO("log file is %p.\n", log_file);
  }

  /* reset rmnet module */
  if( MMDATA_SUCCESS != mmdata_rmnet_cfg() )
  {
    MMDATA_ERR("failed on mmdata_rmnet_reset\n");
    goto exit;
  }

  MMDATA_INFO("%s :waiting on while !!!",__func__);
  while(true){
  sleep(1);
  }

exit:
  return MMDATA_SUCCESS;

}

int mmdata_strcpy(char *dest, const char *src, int dsize)
{
  const char *osrc = src;
  int left = dsize;

  if (left != 0)
  {
    while (--left != 0)
    {
      if ((*dest++ = *src++) == '\0')
       break;
    }
  }

  if (left == 0)
  {
    if (dsize != 0)
      *dest = '\0';
    while (*src++);
  }
  return(src - osrc - 1);
}

