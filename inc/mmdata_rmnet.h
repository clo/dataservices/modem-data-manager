/******************************************************************************

Copyright (c) 2020 The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of The Linux Foundation nor the names of its
          contributors may be used to endorse or promote products derived
          from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/


#ifndef __MMDATA_RMNET_H__
#define __MMDATA_RMNET_H__

#include "mmdata_main.h"

/* ==========================================================================*/

#define MMDATA_MAX_PROC_LEN       (200)
#define MMDATA_QMAPv5             (5)
#define MMDATA_QMAPv4             (4)
#define MMDATA_QMAPv5_STR         "MAPv5"
typedef union
{
  int intval;
  unsigned long longval;
  char strval[20];
} mmdata_val_t;

typedef enum
{
  MMDATA_TYPE_INVALID = -1,
  MMDATA_TYPE_INT,
  MMDATA_TYPE_HEX,
  MMDATA_TYPE_LONG,
  MMDATA_TYPE_STR,
  MMDATA_TYPE_MAX
} mmdata_val_type_t;

typedef struct
{
  mmdata_val_t       config_val;
  mmdata_val_type_t  config_type;
} mmdata_procsys_cfg_t;

int
mmdata_rmnet_configure_links
(
  void
);

int
mmdata_rmnet_change_link_configuration
(
  int flags
);

int
mmdata_rmnet_remove_links
(
  void
);


#endif  /* __MMDATA_RMNET_H__ */

