/******************************************************************************

Copyright (c) 2020 The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above
          copyright notice, this list of conditions and the following
          disclaimer in the documentation and/or other materials provided
          with the distribution.
        * Neither the name of The Linux Foundation nor the names of its
          contributors may be used to endorse or promote products derived
          from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/
#ifndef __MMDATA_MAIN_H__
#define __MMDATA_MAIN_H__

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include<stdlib.h>

#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <inttypes.h>
#include <assert.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include <linux/if.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/udp.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>

#include "libqrtr.h"

#define MMDATA_SUCCESS    (0)
#define MMDATA_FAILURE    (-1)
#define TRUE    (1)
#define FALSE   (0)

#define MMDATA_RMNET_IPA               "rmnet_ipa0"
#define MMDATA_RMNET_0                 "rmnet_data0"
#define MMDATA_MAX_RETRIES        (120)
#define MMDATA_MAX_IF_LEN         (32)

/* Logging */
#define printf          LOGI
#define MMDATA_INFO     LOGI
#define MMDATA_ERR      LOGE
#define MMDATA_ERRNO    LOGE_ERRNO

extern FILE* log_file;

#define LOGI(fmt, ...)          do { fprintf(log_file, "I    |MMDATA: %s " fmt "\n",__func__,  ##__VA_ARGS__); } while (0)
#define LOGE_ERRNO(fmt, ...)    do { fprintf(log_file, "E   |MMDATA: %s " fmt ": %s\n",__func__, ##__VA_ARGS__, strerror(errno)); } while (0)
#define LOGE(fmt, ...)          do { fprintf(log_file, "E   |MMDATA: %s " fmt "\n",__func__, ##__VA_ARGS__); } while (0)

#define mmdata_malloc(size)  malloc(size)
#define mmdata_free(ptr)     if( ptr ) { free(ptr); ptr = NULL; }

#define MMDATA_ASSERT(a)                                            \
       if (!(a))                                                    \
       {                                                            \
        fprintf(stderr, "%s, %d: assertion (a) failed!",__FILE__,   \
             __LINE__);                                             \
        abort();                                                    \
       }


int mmdata_strcpy(char *dest, const char *src, int dsize);

#endif /* __MMDATA_MAIN_H__ */
